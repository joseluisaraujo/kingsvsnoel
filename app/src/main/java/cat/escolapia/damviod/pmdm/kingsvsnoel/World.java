package cat.escolapia.damviod.pmdm.kingsvsnoel;

import java.util.Random;

/**
 * Created by joseluis.araujo on 11/01/2017.
 */
public class World {
    static final int WORLD_WIDTH = 10;
    static final int WORLD_HEIGHT = 15;
    static final int SCORE_INCREMENT = 10;
    static final float TICK_INITIAL = 0.5f;
    static final float TICK_DECREMENT = 0.02f;

    public King king;


    public Door door1;
   // public Door door2;
   // public Door door3;
    public Santa[] santa;
    public boolean gameOver = false;
    public boolean Victori = false;
    public int score = 0;

    boolean fields[][] = new boolean[WORLD_WIDTH][WORLD_HEIGHT];
    Random random = new Random();
    float tickTime = 0;
    float tick = TICK_INITIAL;
    int countKing = 1;

    public World() {

        santa = new Santa[13];
        for(int i = 0; i<santa.length; i++){
            int santaX = random.nextInt(WORLD_WIDTH);
            santa[i] = new Santa(santaX,1+i);
        }
        int doorXrandom = random.nextInt(WORLD_WIDTH);
        king = new King(4,14,1);
        door1 = new Door(doorXrandom,0);
      //  door2 = new Door(2,5);
        //door3 = new Door(2,8);

    }


    public void update(float deltaTime) {
        if (gameOver) return;

        tickTime += deltaTime;
        while (tickTime > tick) {
            tickTime -= tick;
            for(int i = 0; i<santa.length; i++) {
                santa[i].moviment();
                santa[i].infintyMap();
                if(santa[i].x == king.x && santa[i].y == king.y){
                    //king = new King(5,14,1);
                    gameOver = true;





                }
            }


                if(door1.x == king.x && door1.y == king.y){
                    for(int i = 0; i<santa.length; i++){
                        int santaX = random.nextInt(WORLD_WIDTH);
                        santa[i] = new Santa(santaX,1+i);
                    }
                    int doorXrandom = random.nextInt(WORLD_WIDTH);
                    door1 = new Door(doorXrandom,0);
                    king = new King(5,14,countKing+1);
                    countKing++;
                }
            if(countKing >= 4){
                Victori = true;
            }

        }
    }

}
