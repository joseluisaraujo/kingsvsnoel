package cat.escolapia.damviod.pmdm.kingsvsnoel;

import java.util.List;

import cat.escolapia.damviod.pmdm.framework.Input;
import cat.escolapia.damviod.pmdm.framework.Screen;
import cat.escolapia.damviod.pmdm.framework.Graphics;
import cat.escolapia.damviod.pmdm.framework.Input.TouchEvent;
import cat.escolapia.damviod.pmdm.framework.Game;

/**
 * Created by joseluis.araujo on 11/01/2017.
 */
public class MainMenu extends Screen {
    public MainMenu(Game game) {super(game);}

    public void update(float deltaTime) {
        Graphics g = game.getGraphics();
        List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
        game.getInput().getKeyEvents();

        int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if(event.type == TouchEvent.TOUCH_UP) {
                //game.setScreen(new GameScene(game));
                if(inBounds(event, 64, 220, 240, 101) ) {// x y posicio width heigh image
                    game.setScreen(new GameScene(game));

                    return;
                }
            }
        }
    }

    private boolean inBounds(TouchEvent event, int x, int y, int width, int height) {
        if(event.x > x && event.x < x + width - 1 &&
                event.y > y && event.y < y + height - 1)
            return true;
        else
            return false;
    }

    public void render(float deltaTime) {
        Graphics g = game.getGraphics();
        g.drawPixmap(Assets.background, 0, 0);
        g.drawPixmap(Assets.start, 64, 220);
    }

    public void pause() {


        }

    public void resume() {

    }

    public void dispose() {

    }
}
