package cat.escolapia.damviod.pmdm.kingsvsnoel;

import cat.escolapia.damviod.pmdm.framework.Graphics;
import cat.escolapia.damviod.pmdm.framework.Game;
import cat.escolapia.damviod.pmdm.framework.Screen;
import cat.escolapia.damviod.pmdm.framework.Graphics.PixmapFormat;

/**
 * Created by joseluis.araujo on 11/01/2017.
 */
public class LoadingScreen extends Screen {
    public LoadingScreen(Game game) {
        super(game);
    }
    public void update(float deltaTime){
        Graphics g = game.getGraphics();
        Assets.background = g.newPixmap("background.png", Graphics.PixmapFormat.RGB565);
        Assets.background2 = g.newPixmap("background2.png", Graphics.PixmapFormat.RGB565);
        Assets.background3 = g.newPixmap("background3.png", Graphics.PixmapFormat.RGB565);
        Assets.king = g.newPixmap("king.png", Graphics.PixmapFormat.RGB565);
        Assets.king2 = g.newPixmap("king2.png", Graphics.PixmapFormat.RGB565);
        Assets.king3 = g.newPixmap("king3.png", Graphics.PixmapFormat.RGB565);
        Assets.start = g.newPixmap("start.png", Graphics.PixmapFormat.RGB565);
        Assets.door1 = g.newPixmap("door1.png", Graphics.PixmapFormat.RGB565);
        Assets.door2 = g.newPixmap("door2.png", Graphics.PixmapFormat.RGB565);
        Assets.door3 = g.newPixmap("door3.png", Graphics.PixmapFormat.RGB565);
        Assets.santa = g.newPixmap("santa.png", Graphics.PixmapFormat.RGB565);
        Assets.gameOver = g.newPixmap("gameOver.png", Graphics.PixmapFormat.RGB565);
        Assets.victori = g.newPixmap("victori.png", Graphics.PixmapFormat.RGB565);
        game.setScreen(new MainMenu(game));
    }
    public void render(float deltaTime){

    }
    public void pause() {

    }
    public void resume(){

    }
    public void dispose(){

    }
}
