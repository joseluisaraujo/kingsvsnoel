package cat.escolapia.damviod.pmdm.kingsvsnoel;

import cat.escolapia.damviod.pmdm.framework.Pixmap;
import cat.escolapia.damviod.pmdm.framework.Music;
import cat.escolapia.damviod.pmdm.framework.Sound;

/**
 * Created by joseluis.araujo on 11/01/2017.
 */
public class Assets {
    public static Pixmap background;
    public static Pixmap background2;
    public static Pixmap background3;
    public static Pixmap king;
    public static Pixmap king2;
    public static Pixmap king3;
    public static Pixmap door1;
    public static Pixmap door2;
    public static Pixmap door3;
    public static Pixmap start;
    public static Pixmap santa;
    public static Pixmap gameOver;
    public static Pixmap victori;

}
