package cat.escolapia.damviod.pmdm.kingsvsnoel;



import com.example.joseluisaraujo.kingsvsnoel.R;


import java.util.List;

import cat.escolapia.damviod.pmdm.framework.Game;
import cat.escolapia.damviod.pmdm.framework.Graphics;
import cat.escolapia.damviod.pmdm.framework.Input.TouchEvent;
import cat.escolapia.damviod.pmdm.framework.Pixmap;
import cat.escolapia.damviod.pmdm.framework.Screen;

public class GameScene extends Screen {

    enum GameState {
        Starting,
        Running,
        Paused,
        GameOver,
        Victori
    }

    GameState state = GameState.Starting;
    World world;

    int xi = 0;
    int xf = 0;
    int yi = 0;
    int yf = 0;
    int distx = 0;
    int disty = 0;

    public GameScene(Game game){
        super(game); world = new World();
    }

    public void update(float deltaTime) {
        List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
        game.getInput().getKeyEvents();

        if(state == GameState.Starting)
            updateStarting(touchEvents);
        if(state == GameState.Running)
            updateRunning(touchEvents, deltaTime);
        if(state == GameState.Paused)
            updatePaused(touchEvents);
        if(state == GameState.GameOver)
            updateGameOver(touchEvents);
        if(state == GameState.Victori)
            updateVictori(touchEvents);
    }
    private void updateStarting(List<TouchEvent> touchEvents) {
        if(touchEvents.size() > 0) state = GameState.Running;
    }
    private void getTouchInitial(TouchEvent event){
        xi = event.x;
        yi = event.y;
    }

    private void getTouchFinal(TouchEvent event){
        xf = event.x;
        yf = event.y;
        distx = xf-xi;
        disty = yf-yi;
    }
    private void updateRunning(List<TouchEvent> touchEvents, float deltaTime) {
        int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if(event.type == TouchEvent.TOUCH_DOWN) {
                getTouchInitial(event);
            }

            if(event.type == TouchEvent.TOUCH_UP) {
                getTouchFinal(event);

                //Slide
                if(distx < -30)
                {
                    world.king.Left();
                }
                else if(distx > 30)
                {
                    world.king.Right();
                }
                else if(disty < -30)
                {
                    world.king.Up();
                }
                else if(disty > 30)
                {
                    world.king.Down();
                }
            }
        }
        world.update(deltaTime);

        if(world.gameOver) {

            state = GameState.GameOver;
        }
        if(world.Victori){
            state = GameState.Victori;
        }

    }
    private void updatePaused(List<TouchEvent> touchEvents) {
        int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if(event.type == TouchEvent.TOUCH_UP) {
               // if(event.x > 0 && event.x <= 320) {
                 //   if(event.y > 0 && event.y <= 480) {

                        state = GameState.Running;
                        return;
                 //   }
//                    if(event.y > 148 && event.y < 196) {

                       // return;
  //                  }
    //            }
            }
        }
    }

    private void updateGameOver(List<TouchEvent> touchEvents) {
        int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if(event.type == TouchEvent.TOUCH_UP) {
                if(event.x >= 0 && event.x <= 320 &&
                        event.y >= 0 && event.y <= 480) {

                    game.setScreen(new MainMenu(game));
                    return;
                }
            }
        }
    }

    private void updateVictori(List<TouchEvent> touchEvents) {
        int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if(event.type == TouchEvent.TOUCH_UP) {
                if(event.x >= 0 && event.x <= 320 &&
                        event.y >= 0 && event.y <= 480) {

                    game.setScreen(new MainMenu(game));
                    return;
                }
            }
        }
    }
    @Override
    public void render(float deltaTime) {
        Graphics g = game.getGraphics();


        if(world.countKing < 3)g.drawPixmap(Assets.background2, 0, 0);
        if(world.countKing == 3)g.drawPixmap(Assets.background3, 0, 0);

        drawWorld(world);
        if(state == GameState.Starting)
            drawStartingUI();
        if(state == GameState.Running)
            drawRunningUI();
        if(state == GameState.Paused)
            drawPausedUI();
        if(state == GameState.GameOver)
            drawGameOverUI();
        if(state == GameState.Victori)
            drawVictoriUI();

        drawText(g, Integer.toString(world.score), g.getWidth() / 2 - Integer.toString(world.score).length() * 20 / 2, g.getHeight() - 42);

    }

    private void drawWorld(World world) {
        Graphics g = game.getGraphics();


        King king = world.king;
        Door door1 = world.door1;
        Santa[] santa = world.santa;

        Pixmap door1Pixmap = null;
        door1Pixmap = Assets.door1;
        //Pixmap door2Pixmap = null;
        /*door2Pixmap = Assets.door2;
        Pixmap door3Pixmap = null;
        door3Pixmap = Assets.door3;*/
        Pixmap santaPixmap = null;
        Pixmap kingPixmap = null;
        if(king.type == 1 || king.type ==4){
            kingPixmap = Assets.king;
        }
        if(king.type == 2){
            kingPixmap = Assets.king2;
        }
        if(king.type == 3){
            kingPixmap = Assets.king3;
        }

        santaPixmap = Assets.santa;


        for(int i = 0; i<santa.length; i++){
            int xs = santa[i].x * 32;
            int ys = santa[i].y * 32;
            g.drawPixmap(santaPixmap, xs , ys );
        }
        int x = king.x * 32;
        int y = king.y * 32;
        g.drawPixmap(kingPixmap, x , y );

        int xd1 = door1.x * 32;
        int yd1 = door1.y * 32;
        //int xd2 = door1.x * 32;
        //int yd2 = door1.y * 32;
        //int xd3 = door1.x * 32;
       // int yd3 = door1.y * 32;
        g.drawPixmap(door1Pixmap, xd1 , yd1 );
       // g.drawPixmap(door2Pixmap, xd2 , yd2 );
        //g.drawPixmap(door3Pixmap, xd3 , yd3 );


    }

    private void drawStartingUI() {
        Graphics g = game.getGraphics();
        //System.out.println("Starting");

    }

    private void drawRunningUI() {
        Graphics g = game.getGraphics();
        // System.out.println("Running");
    }

    private void drawPausedUI() {
        Graphics g = game.getGraphics();

    }

    private void drawGameOverUI() {

            Graphics g = game.getGraphics();
            g.drawPixmap(Assets.gameOver, 35, 100);


    }

    private void drawVictoriUI() {
        Graphics g = game.getGraphics();
        g.drawPixmap(Assets.victori, 1, 100);


    }

    public void drawText(Graphics g, String line, int x, int y) {
        int len = line.length();
        for (int i = 0; i < len; i++) {
            char character = line.charAt(i);

            if (character == ' ') {
                x += 20;
                continue;
            }

            int srcX = 0;
            int srcWidth = 0;
            if (character == '.') {
                srcX = 200;
                srcWidth = 10;
            } else {
                srcX = (character - '0') * 20;
                srcWidth = 20;
            }


            x += srcWidth;
        }
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
