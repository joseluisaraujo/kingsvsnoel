package cat.escolapia.damviod.pmdm.kingsvsnoel;

/**
 * Created by joseluis.araujo on 11/01/2017.
 */
public class King {
    public int x, y;
    int type;
    public King(int x, int y, int type){
        this.x = x;
        this.y = y;
        this.type = type;
    }

    public void Right(){
        x +=1;
    }
    public void Left(){
        x -=1;
    }
    public void Up(){
        y -=1;
    }
    public void Down(){
        y +=1;
    }



}
